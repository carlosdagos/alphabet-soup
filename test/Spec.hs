import           AlphabetSoup          (AlphabetSoup, Message, mkMessage,
                                        testSoup)
import           LazyAlphabetSoup      (testLazySoup)
import           PagedAlphabetSoup     (testPagedSoup)
import           Test.Tasty
import           Test.Tasty.HUnit
import qualified Test.Tasty.QuickCheck as QC

type SoupFunction = String -> AlphabetSoup -> Bool


main :: IO ()
main = defaultMain alphabetSoupTests

-- The empty soup, and the empty message
e :: Message
e = ""

alphabetSoupTests :: TestTree
alphabetSoupTests = testGroup "Alphabet Soup tests"
      [ testGroup "Default Alphabet Soup tests"
            [ properties testSoup, units testSoup ]
      , testGroup "Lazy Alphabet Soup tests"
            [ properties testLazySoup, units testLazySoup ]
      , testGroup "Paged Alphabet Soup tests"
            [ properties testPagedSoup', units testPagedSoup' ]
      ]
    where
        -- NOTICE: Tests are carried out for paged soups of 100 chars
        -- per page
        testPagedSoup' :: SoupFunction
        testPagedSoup' message soup = testPagedSoup message soup 100


properties :: SoupFunction -> TestTree
properties soupFunc =
    localOption (QC.QuickCheckTests 100000) $ testGroup "Property Tests"
    [ QC.testProperty "Empty soup always returns false for non-empty message" $
        \message -> mkMessage message == e || soupFunc message e == False
    , QC.testProperty "Empty message always returns true" $
        \soup -> soupFunc e soup == True
    , QC.testProperty "For any soup, it is a message of itself" $
        \soup -> soupFunc soup soup == True
    ]


units :: SoupFunction -> TestTree
units soupFunc =
    testGroup "Unit Tests"
    [ testCase "A message with spaces is equivalent to without" $
        let soup = "SECAPSHTIWEGASSEM"
        in
            (soupFunc "MESSAGE WITH SPACES" soup) @?=
            (soupFunc "MESSAGEWITHSPACES" soup)
    , testCase "Soups are case-sensitive" $
        soupFunc "message" "MESSAGE" @?= False
    , testCase "Cannot build a message completely different from a soup" $
        soupFunc "XYZ" "ABC" @?= False
    , testCase "Cannot build a message with characters not in the soup" $
        soupFunc "THIS IS A MESSAGE XYZ" "THISISAMESSAGE" @?= False
    ]
