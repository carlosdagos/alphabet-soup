Alphabet Soup
=============

This folder contains Haskell code that implements the test task.
I wrote it in Haskell since my Common Lisp is a bit rusty :)
I hope it's okay.

## To quickly run everything

I've added a quick `run.sh` script that will execute everything needed, plus
a couple of tests.

You can also run `run.sh --only-tests` to only run the tests, but it will skip
property tests and benchmarks.

## Properties

As specified on an email to Andrew, I've come up with the following
properties for the solution:

### Definitions

For the uppercase letters of the English alphabet

    A = {A,B,C,..,Z}, |A| = 26.

A soup of length _n_ ∈ ℕ is just any combination of _A_.

    soup = A_n

And for any _n_, any soup will come from the set

    soups = {e} ∪ { ∀ n ∈ ℕ , ∪ A_n }

Where _e_ denotes an empty soup. So soups can be empty.

Messages are like soups with the exception that they can contain
spaces. So any message to be checked against `soups` is defined by
a regex

    message = /[A-Z\s]*/

Where `\s` means "any whitespace character". So a message like
`"THIS IS A MESSAGE"` is the same as `"THISISAMESSAGE"` and in fact
equivalent to any permutation of the letters contained within
the message. And a message containing only whitespace characters
is the same as an empty message, which is the same as _e_ (the empty soup).

### Properties

The solution for the test task is called `testSoup`, which accepts
a message and a `soup` (`soup ∈ soups`). All mentions of `message` below
contain no whitespace characters, since they're equivalent to messages
with them.

1) For a non-empty message, an empty soup will always return false.

```
∀ message, message ≠ e, testSoup message e = False
```

2) For any soup, an empty message will always return true.

```
∀ soup ∈ soups, testSoup e soup = True
```

3) For any soup, it can be a message of itself.

```
∀ soup ∈ soups, testSoup soup soup = True
```

These properties can be checked against the implementation.

## Implementation overview

For a soup of size `m`. The implementation will build a map `M_m` such as

    for each unique char in m
        Map { key = char, value = occurrences of char in m }

And for a _valid_ message of size `n` **without whitespaces**, we build
a map `M_n` such as

    for each unique char in n
        Map { key = char, value = occurrences of char in n }

These maps essentially define a set of tuples. We can check that
`M_n` is a subset or equal of `M_m`,  to determine that the characters of `n`
appear in `m`

    M_n ⊆ M_m

And then keeping only the tuples of `M_n` where the corresponding tuple
in `M_m` has at least as many occurrences of each `char`.

In other words:

- Count each letter in `n` and put it into a map (`O(n * log(n))`)
- Count each letter in `m` and put it into a map (`O(m * log(m))`)
- Check that `M_n` is a subset of `M_m` using a custom function
  where each char in `n` appears in `m` at least as many times (`O(n'+m')`)

Where `n'` = `|M_n|` and `m' = |M_m|`. And `|M_n| ≤ |A|`, and `|M_m| ≤ |A|`.

The steps above describe our `testSoup` function.

## Code structure

### src

Contains the library code. This is the folder where you want to look for the
implementation of the solution, specifically the `AlphabetSoup.hs` file. I
tried to keep the Haskell code as lightweight as possible.

### test

Contains a minimalistic suite of property and unit tests. I use property tests
to verify that my program holds for the assumptions I've made. I use unit
testing to verify smaller scenarios.

### bench

Contains code that will execute a benchmark for random sizes of alphabet soups.
The benchmarks are ran for messages and soups that are the same, to
avoid generating random soups and messages. This way we only generate random
soups.

## Commands available

I used the [`stack`](https://docs.haskellstack.org/en/stable/README/) build
tool. This is required to build and run the code. I unsuccessfully tried to
supply you with a Dockerfile but failed :)

### `stack test`

Will execute the property tests and unit tests:

[![asciicast](https://asciinema.org/a/b0ip6s1zqndyj6ilqw4gggo9y.png)](https://asciinema.org/a/b0ip6s1zqndyj6ilqw4gggo9y)

Property tests are checked against 100000 scenarios, so they can take a bit
to complete.

### `stack bench`

Will execute the benchmarks for the implementation against a number of
random soups of specified lengths. The output of the bechmark is within
this folder and is called `soup_benchmarks.html`.

[![asciicast](https://asciinema.org/a/8txvvvl0zspxi2ipo7z4elwnz.png)](https://asciinema.org/a/8txvvvl0zspxi2ipo7z4elwnz)

Benchmarks are boring to look at. Some of them are for soups of millions of
characters in size, so they take a while.

### `stack exec`

Also provided is a command line executable. You can check its usage
by running

    $ stack exec alphabet-soup -- -h
    Usage: alphabet-soup (-m|--message MESSAGE) (-s|--soup SOUP)
                         (-o|--operating-mode MODE) [-p|--page-size PAGESIZE]

    Available options:
      -m,--message MESSAGE     Message to test
      -s,--soup SOUP           Soup to test against the provided message
      -o,--operating-mode MODE Specifies the mode of running the soup: default,
                               lazy, paged
      -p,--page-size PAGESIZE  If the mode is 'paged', specify the page size
      -h,--help                Show this help text

[![asciicast](https://asciinema.org/a/ecih7exu1pzx270dvgwjwf9fm.png)](https://asciinema.org/a/ecih7exu1pzx270dvgwjwf9fm)

#### Exit codes

The CLI will exit with the following codes, depending on the
scenario

- `0`: Soup Success. The message can be built from the soup.
- `1`: Soup Failure. The message cannot be built from the soup.
- `127`: CLI error. The CLI failed due to wrong arguments to test a soup.

## Assumptions

- I tried to sanitize input, but mostly I assume that people will enter
characters in the english alphabet or regular spaces. It shouldn't matter
to the solution, but I can't make any claims about the output of the program
otherwise. I did this on purpose to keep the code clean of boilerplate
sanitisers.

- I use the very inefficient Haskell `String` type, which is easy to reason
about because it's defined as an array of `Char`. We could get faster
performance using a strict `ByteString`, but again this would reduce the
readability of the code for non Haskellers. In fact I hope the solution
here is readable enough. If not, please tell me.

## Room for improvement

I believe that building the maps of unique characters can be a bit more
efficient if we sort the array first, but that might have an upper bound.
It definitely would require some research.

Another possible non-Haskell-specific optimisation could be to do ordering
and take some ideas from substring searching with some tweaks. Again, this
would be another thing that I'd have to research.

## Thanks!

Thanks for the opportunity, talk soon :)
