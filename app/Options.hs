module Options
      ( Mode(..)
      , Options(..)
      , soupCliOptions
      ) where

import           Options.Applicative

data Mode = Default
          | Lazy
          | Paged
          deriving (Show, Eq)

data Options = Options { message  :: String
                       , soup     :: String
                       , mode     :: Mode
                       , pageSize :: Maybe Int
                       }
                       deriving Show


soupCliOptions' :: Parser Options
soupCliOptions' = Options
      <$> strOption
          ( long "message"
         <> short 'm'
         <> metavar "MESSAGE"
         <> help "Message to test" )
      <*> strOption
          ( long "soup"
         <> short 's'
         <> metavar "SOUP"
         <> help "Soup to test against the provided message" )
      <*> (parseMode <$> ( strOption
          ( long "operating-mode"
         <> short 'o'
         <> metavar "MODE"
         <> help "Specifies the mode of running the soup: default, lazy, paged" )))
      <*> ((fmap . fmap) read ((optional . strOption)
          ( long "page-size"
         <> short 'p'
         <> metavar "PAGESIZE"
         <> help "If the mode is 'paged', specify the page size" )))
    where
        parseMode :: String -> Mode
        parseMode x = case x of
            "default" -> Default
            "lazy"    -> Lazy
            "paged"   -> Paged
            _         -> error "Unknown operating mode. Run with -h."

soupCliOptions :: ParserInfo Options
soupCliOptions = info (soupCliOptions' <**> helper) idm
