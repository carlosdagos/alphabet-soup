{-# LANGUAGE RecordWildCards #-}

module Main where

import qualified AlphabetSoup        as AS
import qualified LazyAlphabetSoup    as LAS
import qualified PagedAlphabetSoup   as PAS

import           Data.Maybe          (fromMaybe)
import           Options             (Mode (..), Options (..), soupCliOptions)
import           Options.Applicative (defaultPrefs, execParserPure,
                                      handleParseResult, infoFailureCode)
import           System.Environment  (getArgs)
import           System.Exit         (ExitCode (..), exitWith)

-- Exits with success
soupMatch :: ExitCode
soupMatch = ExitSuccess

-- Exits with no-match error
soupDoesntMatch :: ExitCode
soupDoesntMatch = ExitFailure 1

-- Exits with CLI error
soupCliError :: Int
soupCliError = 127

-- Executes an action and exits with the provided exit code
exitWithCode :: ExitCode -> IO a -> IO a
exitWithCode code action = action >> exitWith code


main :: IO ()
main = do
    -- Get the arguments and return options
    let sOpts = soupCliOptions { infoFailureCode = soupCliError }
    Options {..} <- execParserPure defaultPrefs sOpts <$> getArgs
                >>= handleParseResult
    -- Define some auxiliary helpers
    let getPageSize = fromMaybe (error "You must define a page size!") pageSize
        testPagedSoup' m s = PAS.testPagedSoup m s getPageSize
    -- Run in mode
    case mode of
      Default -> runTestSoup AS.testSoup message soup
      Lazy    -> runTestSoup LAS.testLazySoup message soup
      Paged   -> runTestSoup testPagedSoup' message soup


-- Run the soup test as a cli app and exits with the pertient code
runTestSoup
    :: (AS.Message -> AS.AlphabetSoup -> Bool)  -- A function that tests a soup
    -> AS.Message                               -- A message
    -> AS.AlphabetSoup                          -- A soup
    -> IO ()
runTestSoup fTestSoup message s =
    let soup     = AS.mkSoup s
        soupTest = fTestSoup message soup
    in
        if soupTest
           then exitWithCode soupMatch $
                    putStrLn "Soup matches"
           else exitWithCode soupDoesntMatch $
                    putStrLn "Soup doesn't match"
