#!/usr/bin/env bash

# 50-character soup generated using system-based randomness
# The system in question is my system
SOUP="ISRGTFKTQPVMICSGJKHBTMWZNWEDUWKITZEAITLQTJKMEDDIT"

# If you want to generate your own soup, you can always use
# SOUP=`cat /dev/random | base64 | tr -dc 'A-Z' | fold -w 50 | head -1`

# Tests the soup defined above against some messages
function run_soup_tests {
    echo "For soup: $SOUP"
    messages=("CARLOS"   "JUAN"        "RAVENPACK"
              "MARBELLA" "SWITZERLAND" "FRANCE"
              "SURFING")

    modes=("default" "lazy" "paged")

    for message in ${messages[@]}
    do
        for mode in ${modes[@]}
        do
            echo -ne "Testing $message with mode $mode: "
            stack exec alphabet-soup -- -m "$message" -s $SOUP -o $mode -p 2
        done
        echo # Blank line to make it readable
    done
}

# Set of commands to run if you don't feel like running it all yourself
function run_test_and_bench {
    echo "Running tests"
    stack test

    echo "Benchmarking"
    stack bench

    run_soup_tests
}

# Check that stack is in the system path
if type stack >/dev/null 2>&1
then
    echo "Setting up project (if required)"
    stack setup

    echo "Building (if required)"
    stack build

    if [[ "$@" == "--only-tests" ]]
    then
        run_soup_tests
    else
        run_test_and_bench
    fi
else
    echo "Error: Install 'stack' first! https://docs.haskellstack.org/en/stable/README/"
    exit 1
fi

