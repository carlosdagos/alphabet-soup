module PagedAlphabetSoup where

import           AlphabetSoup (AlphabetSoup, PageSize, Summary, mkMessage,
                               mkSoup, modifySummary', summarize, testSummaries)

testPagedSoup :: String -> AlphabetSoup -> PageSize -> Bool
testPagedSoup msg soup pageSize =
    -- Test first the amount of the page
    let message        = mkMessage msg
        initialSummary = summarize . mkSoup . take pageSize $ soup
        messageSummary = summarize message
    in
        testLazySoup' messageSummary (drop pageSize soup) initialSummary
    where
        -- Go into a loop page by page until we match or we're out of chars
        testLazySoup' :: Summary -> AlphabetSoup -> Summary -> Bool
        testLazySoup' messageSummary remainingSoup@(_:_) soupSummary =
            testSummaries messageSummary soupSummary ||
               let newPage        = take pageSize remainingSoup
                   remainingPages = drop pageSize remainingSoup
                   newSoupSummary = modifySummary' soupSummary newPage
               in
                   testLazySoup' messageSummary remainingPages newSoupSummary
        -- If we're out of chars, just try to match summaries
        testLazySoup' messageSummary [] soupSummary =
            testSummaries messageSummary soupSummary

