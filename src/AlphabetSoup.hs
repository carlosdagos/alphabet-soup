{-# LANGUAGE MultiWayIf #-}

module AlphabetSoup where


import           Data.List       (foldl')
import qualified Data.Map.Strict as M


type AlphabetSoup = String
type Message      = String
-- ^ Represented as a string
type Summary = M.Map Char Int
-- ^ Represented as a strict map with a counter for each Char
type PageSize = Int
-- ^ Page size is just an integer

-- /O(n)/
-- Filters out spaces to make a soup
mkSoup :: String -> AlphabetSoup
mkSoup = filter (/= ' ')

-- /O(log n)/
-- Makes a valid message as per the definitions
mkMessage :: Message -> Message
mkMessage = filter (/= ' ')

-- /O(log n)/
-- Modifies a Summary
modifySummary :: Summary -> Char -> Summary
modifySummary map' el = if el `M.member` map'
                           then M.adjust (+1) el map'
                           else M.insert el 1 map'

-- /O(n*log n)/
-- Modifies a summary using a string rather than a single char
modifySummary' :: Summary -> String -> Summary
modifySummary' map' []     = map'
modifySummary' map' (x:[]) = modifySummary map' x
modifySummary' map' (x:xs) = modifySummary' (modifySummary map' x) xs

-- /O(n*log n)/
-- Reduces a list into a summary
summarize :: AlphabetSoup -> Summary
summarize = foldl' modifySummary mempty

-- /O(n'+m')/ where
--   n' = size of the summary of n
--   m' = size of the summary of m
--
-- Using sets (represented as maps), and a custom intersection, it checks that
-- all the characters in the first set appear in the second with less than or
-- an equal amount of characters
testSoup :: Message -> AlphabetSoup -> Bool
testSoup msg soup =
    let message          = mkMessage msg
        soupSummary      = summarize soup
        msgSummary       = summarize message
    in
        testSummaries msgSummary soupSummary

-- /O(n+m)/
-- Tests that the summary in the first argumetn is a subset of
-- the summary in the second argument
testSummaries :: Summary -> Summary -> Bool
testSummaries msgSummary soupSummary =
    let commonLetters    = M.intersectionWith (<=) msgSummary soupSummary
        allLettersInSoup = M.keysSet msgSummary == M.keysSet commonLetters
    in
        if -- Property 2
           | msgSummary == mempty    -> True
           -- Letters in message not in the soup
           | not allLettersInSoup    -> False
           -- Property 1
           | commonLetters == mempty -> False
           -- Check that all occurrences are correct
           | otherwise               -> all snd $ M.toList commonLetters


