module LazyAlphabetSoup where

import           AlphabetSoup (AlphabetSoup, PageSize, Summary, mkMessage,
                               mkSoup, modifySummary, summarize, testSummaries)

testLazySoup :: String -> AlphabetSoup -> Bool
testLazySoup msg soup = testLazySoupWithPageSize msg soup (length msg)


testLazySoupWithPageSize :: String -> AlphabetSoup -> PageSize -> Bool
testLazySoupWithPageSize msg soup pageSize =
    -- Test first the amount of the page
    let message        = mkMessage msg
        initialSummary = summarize . mkSoup . take pageSize $ soup
        messageSummary = summarize message
    in
        testLazySoup' messageSummary (drop pageSize soup) initialSummary
    where
        -- Go into a loop char by char until we match or we're out of chars
        testLazySoup' :: Summary -> AlphabetSoup -> Summary -> Bool
        testLazySoup' messageSummary (el:soupRest) soupSummary =
            testSummaries messageSummary soupSummary ||
               let newSoupSummary = modifySummary soupSummary el
               in
                   testLazySoup' messageSummary soupRest newSoupSummary
        -- If we're out of chars, just try to match summaries
        testLazySoup' messageSummary [] soupSummary =
            testSummaries messageSummary soupSummary

