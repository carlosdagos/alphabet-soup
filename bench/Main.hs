module Main where

import           AlphabetSoup      (AlphabetSoup, testSoup)
import           Control.Monad     (replicateM)
import           Criterion.Main    (Benchmark, bench, defaultMain, env, whnf)
import           Data.Monoid       ((<>))
import           LazyAlphabetSoup  (testLazySoup)
import           PagedAlphabetSoup (testPagedSoup)
import           Test.QuickCheck   (Gen, elements, generate, infiniteListOf)

validSoupChars :: Gen Char
validSoupChars = elements ['A'..'Z']

-- Generates a random soup of a specific size
genSafeStringOfSize :: Int -> Gen String
genSafeStringOfSize n = replicateM n validSoupChars

-- Generates a random infinite soup
genInfiniteSafeString :: Gen String
genInfiniteSafeString = infiniteListOf validSoupChars

-- For a parameter that indicates the length of a soup, return a Benchmark
-- for a "strict soup" test
defaultSoupBench :: Int -> Benchmark
defaultSoupBench n =
    env (generate $ genSafeStringOfSize n) $ \soup ->
       bench ("Soup of size " <> show n) $ whnf (uncurry testSoup) (soup, soup)


-- For a parameter that indicates the length of a soup, return a Benchmark
-- for a "lazy soup" test. Because the soups are the same, the results
-- should be the same as for a "strict" soup test.
lazySoupBench :: Int -> Benchmark
lazySoupBench n =
    env (generate $ genSafeStringOfSize n) $ \soup ->
        bench ("Lazy Soup of size " <> show n) $
            whnf (uncurry testLazySoup) (soup, soup)

-- For a parameter that indicates the length of a soup, return a Benchmark
-- for a "paged soup" test. This should return the same timings as
-- `defaultSoupBench` and as `lazySoupBench`.
pagedSoupBench :: Int -> Benchmark
pagedSoupBench n =
    env (generate $ genSafeStringOfSize n) $ \soup ->
        bench ("Paged Soup of size " <> show n) $
            whnf (uncurry testPagedSoup') (soup, soup)
    where
        testPagedSoup' message soup
            = testPagedSoup message soup (length message)

defaultSoupBenchFixedMsg :: Int ->  Benchmark
defaultSoupBenchFixedMsg n =
    env (generate $ genSafeStringOfSize n) $ \soup ->
        bench message $
            whnf (uncurry testSoup) (take 1000 soup, soup)
    where
        message = "Soup of size "
                <> show n
                <> " with fixed message of 1000 chars"

-- For a parameter that indicates the length of a soup, return a Benchmark
-- for a "lazy soup" test. Different to the solution in `soupBench`,
-- we should see  the same execution time for all benchmarks.
lazySoupBenchFixedMsg :: Int -> Benchmark
lazySoupBenchFixedMsg n =
    env (generate $ genSafeStringOfSize n) $ \soup ->
        bench message $
            whnf (uncurry testLazySoup) (take 1000 soup, soup)
    where
        message :: String
        message = "Lazy Soup of size "
               <> show n
               <> " with fixed message of 1000 chars"

-- For a parameter that indicates the length of a soup, return a Benchmark
-- for a "paged soup" test with a fixed page size of 100. We should see the
-- same timings for the first two benchmarks, and then start changing on the
-- last three benchmarks.
pagedSoupBenchFixedMsg :: Int -> Benchmark
pagedSoupBenchFixedMsg n =
    env (generate $ genSafeStringOfSize n) $ \soup ->
        bench message $
            whnf (uncurry testPagedSoup') (take 1000 soup, soup)
    where
        message :: String
        message = "Paged Soup of size "
               <> show n
               <> " with fixed page of 1000 chars"
        testPagedSoup' :: String -> AlphabetSoup -> Bool
        testPagedSoup' msg soup = testPagedSoup msg soup 100

-- Create a set for specific sizes of a soup
runBenchmark :: IO ()
runBenchmark = defaultMain $ -- Default benchmarks
                             defaultSoupBenches
                          <> lazySoupBenches
                          <> pagedSoupBenches
                             -- Alternative benchmarks
                          <> defaultSoupBenchesWithFixedMessage
                          <> lazySoupBenchesWithFixedMessage
                          <> pagedSoupBenchesWithFixedMessage
    where
        -- Default benchmark definitions
        sizes :: [Int]
        sizes = [100, 1000, 100000, 1000000, 10000000]

        defaultSoupBenches :: [Benchmark]
        defaultSoupBenches =
            map defaultSoupBench sizes

        lazySoupBenches :: [Benchmark]
        lazySoupBenches =
            map lazySoupBench sizes

        pagedSoupBenches :: [Benchmark]
        pagedSoupBenches =
            map pagedSoupBench sizes

        -- Alternative benchmark definitions
        alternativeSizes :: [Int]
        alternativeSizes = drop 2 sizes

        defaultSoupBenchesWithFixedMessage :: [Benchmark]
        defaultSoupBenchesWithFixedMessage =
            map defaultSoupBenchFixedMsg alternativeSizes

        lazySoupBenchesWithFixedMessage :: [Benchmark]
        lazySoupBenchesWithFixedMessage =
            map lazySoupBenchFixedMsg alternativeSizes

        pagedSoupBenchesWithFixedMessage :: [Benchmark]
        pagedSoupBenchesWithFixedMessage =
            map pagedSoupBenchFixedMsg alternativeSizes


-- Main function for benchmarks
main :: IO ()
main = do
    putStrLn "Running benchmarks"
    runBenchmark
